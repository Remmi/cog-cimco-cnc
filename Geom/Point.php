<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 16.10.2014
 * Time: 21:25
 */

namespace Geom;

/**
 * Class Point - класс для работы с точками
 * @package Geom
 */
class Point {
    /**
     * @var float - координата по оси X
     */
    private $x;
    /**
     * @var float - координата по оси Y
     */
    private $y;

    /**
     * @param float $x - координата X
     * @param float $y - координата Y
     */
    public function __construct($x=0.0, $y=0.0) {
        if (is_numeric($x) && is_numeric($y)) {
            $this->x = (float)$x;
            $this->y = (float)$y;
        }
    }

    /**
     * @return float - возвращает координату по X
     */
    public function getX() {
        return $this->x;
    }

    /**
     * Записывает новое значение координаты X
     * @param $x - новое значение
     * @return bool - успешность записи
     */
    public function setX($x) {
        if (is_numeric($x)) {
            $this->x = (float)$x;
            return true;
        }
        return false;
    }

    /**
     * @return float - возвращает координату по Y
     */
    public function getY() {
        return $this->y;
    }

    /**
     * Записывает новое значение координаты Y
     * @param $y - новое значение
     * @return bool - успешность записи
     */
    public function setY($y) {
        if (is_numeric($y)) {
            $this->y = (float)$y;
            return true;
        }
        return false;
    }

    /**
     * Делает сдвиг точки на заданную величину
     * @param float $x - сдвиг по X
     * @param float $y - сдвиг по Y
     * @return bool - успешность сдвига
     */
    public function movePoint($x=0.0, $y=0.0) {
        if (is_numeric($x) && is_numeric($y)) {
            $this->x += (float)$x;
            $this->y += (float)$y;
            return true;
        }
        return false;
    }

    public static function getDistanceBetweenPoints($x1, $y1, $x2, $y2) {
        return sqrt(($x2-$x1)*($x2-$x1)+($y2-$y1)*($y2-$y1));
    }

    /**
     * @param float $x - X точки
     * @param float $y - Y точки
     * @return float - расстояние до точки
     */
    public function getDistance($x=0.0, $y=0.0) {
        $x0 = $this->getX();
        $y0 = $this->getY();
        return self::getDistanceBetweenPoints($x0, $y0, $x, $y);
    }

    public static function getDistanceBetweenPointAndLine($x, $y, $a, $b, $c) {
        $d = abs($a*$x+$b*$y+$c)/sqrt(($a*$a+$b*$b));
        return $d;
    }

    public function getDistanceToLine($a, $b, $c) {
        return self::getDistanceBetweenPointAndLine(
            $this->getX(),
            $this->getY(),
            $a, $b, $c
        );
    }

    public static function getVerticalCrossWithPoint($x0, $y0, $x1, $y1, $x2, $y2) {
        if ($x1 == $x2) {
            $xp = $x1;
            $yp = $y0;
        } elseif ($y1 == $y2) {
            $xp = $x0;
            $yp = $x1;
        } else {
            $xp = $x1*($y2-$y1)*($y2-$y1) + $x0*($x2-$x1)*($x2-$x1) + ($x2-$x1)*($y2-$y1)*($y0-$y1);
            $xp = $xp / (($y2-$y1)*($y2-$y1)+($x2-$x1)*($x2-$x1));
            $yp = ($x2-$x1)*($x0-$xp)/($y2-$y1);
            $yp = $yp + $y0;
        }
        return new Point($xp, $yp);
    }

    public function getVerticalCrossToLine($x1, $y1, $x2, $y2) {
        return self::getVerticalCrossWithPoint(
            $this->getX(), $this->getY(), $x1, $y1, $x2, $y2
        );
    }

    public function isPointOnLine($a, $b, $c, $e=0.0) {
        $d = $this->getDistanceToLine($a, $b, $c);
        return ($d <= $e);
    }

    public static function getDistanceBetweenPointAndCircle($x, $y, $r, $x0=0.0, $y0=0.0) {
        $d = sqrt(($x-$x0)*($x-$x0)+($y-$y0)*($y-$y0));
        return abs($d-$r);
    }

    public static function isPointOnCircle($x, $y, $r, $x0=0.0, $y0=0.0, $e=0.01) {
        $d = self::getDistanceBetweenPointAndCircle($x, $y, $r, $x0, $y0);
        return ($d <= $e);
    }

    public function isOnCircle($r, $x0=0.0, $y0=0.0, $e=0.01) {
        return self::isPointOnCircle(
            $this->getX(), $this->getY(), $r, $x0, $y0, $e
        );
    }

    /**
     * Проверяет, является ли объект точкой
     * @param $point - переданный объект
     * @return bool - результат проверки на точку
     */
    public static function isPoint($point) {
        return ($point instanceof self);
    }
} 