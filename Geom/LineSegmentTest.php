<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 18.10.2014
 * Time: 13:18
 */

namespace Geom;

include 'Point.php';
include 'Line.php';
include 'LineSegment.php';

class LineSegmentTest extends \PHPUnit_Framework_TestCase {
    public function testLineSegment() {
        $newLS = new LineSegment(1.61, 4.33, 6.004, -5.1);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(1.61, $sPoint->getX());
        $this->assertEquals(4.33, $sPoint->getY());
        $this->assertEquals(6.004, $ePoint->getX());
        $this->assertEquals(-5.1, $ePoint->getY());

        $sPoint = new Point(4.82, -1.95);
        $ePoint = new Point(-7.24, 5.93);
        $newLS = new LineSegment($sPoint, $ePoint);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(4.82, $sPoint->getX());
        $this->assertEquals(-1.95, $sPoint->getY());
        $this->assertEquals(-7.24, $ePoint->getX());
        $this->assertEquals(5.93, $ePoint->getY());

        $sPoint = new Point(513.11, -543.95);
        $newLS = new LineSegment($sPoint, -54.14, 24.2);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(513.11, $sPoint->getX());
        $this->assertEquals(-543.95, $sPoint->getY());
        $this->assertEquals(-54.14, $ePoint->getX());
        $this->assertEquals(24.2, $ePoint->getY());

        $ePoint = new Point(35.3, -150.2);
        $newLS = new LineSegment(46.8, 35.1, $ePoint);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(46.8, $sPoint->getX());
        $this->assertEquals(35.1, $sPoint->getY());
        $this->assertEquals(35.3, $ePoint->getX());
        $this->assertEquals(-150.2, $ePoint->getY());
    }

    public function testLineSegmentSetMethods() {
        $newLS = new LineSegment(1.61, 4.33, 6.004, -5.1);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(1.61, $sPoint->getX());
        $this->assertEquals(4.33, $sPoint->getY());
        $this->assertEquals(6.004, $ePoint->getX());
        $this->assertEquals(-5.1, $ePoint->getY());
        $sPoint = new Point(5.9, 3.16);
        $ePoint = new Point(8.7, 77.1);
        $newLS->setStartPoint($sPoint);
        $newLS->setEndPoint($ePoint);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(5.9, $sPoint->getX());
        $this->assertEquals(3.16, $sPoint->getY());
        $this->assertEquals(8.7, $ePoint->getX());
        $this->assertEquals(77.1, $ePoint->getY());
    }

    public function testLineSegmentMoveMethod() {
        $newLS = new LineSegment(1.61, 4.33, 6.004, -5.1);
        $newLS->moveLineSegment(3, 5);
        $sPoint = $newLS->getStartPoint();
        $ePoint = $newLS->getEndPoint();
        $this->assertEquals(4.61, $sPoint->getX());
        $this->assertEquals(9.33, $sPoint->getY());
        $this->assertEquals(9.004, $ePoint->getX());
        $this->assertEquals(-0.1, $ePoint->getY());
    }

    public function testLineSegmentDistanceMethod() {
        $newLS = new LineSegment(1.61, 4.33, 6.004, -5.1);
        $r = LineSegment::getDistance($newLS->getStartPoint(), $newLS->getEndPoint());
        $this->assertEquals(10.40346, $r, "Wrong distance", 0.00001);
    }

    public function testLineSegmentLengthMethod() {
        $newLS = new LineSegment(1, 1, 5, 1);
        $this->assertEquals(4, $newLS->getLength());
    }

    public function testLineSegmentOffsetMethod() {
        $newLS = new LineSegment(1, 1, 5, 1);
        $this->assertEquals(0, $newLS->getOffsetX());

        $newLS = new LineSegment(1, 2, 3, 4);
        $this->assertEquals(-1, $newLS->getOffsetX());
    }

    public function testLineSegmentMiddleMethod() {
        $newLS = new LineSegment(1, 1, 3, 3);
        $middle = $newLS->getMiddlePoint();
        $this->assertEquals(true, Point::isPoint($middle));
        $this->assertEquals(new Point(2,2), $middle);
        $this->assertEquals(2, $middle->getX());
        $this->assertEquals(2, $middle->getY());
    }
}
 