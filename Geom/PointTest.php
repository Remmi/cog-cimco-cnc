<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 16.10.2014
 * Time: 21:47
 */

namespace Geom;

include 'Point.php';

class PointTest extends \PHPUnit_Framework_TestCase {
    public function testPoint() {
        // точка должна создаваться с нулями
        $point = new Point();
        $this->assertEquals(0, $point->getX());
        $this->assertEquals(0, $point->getY());
        // и с теми координатами, которые мы задаем
        $point = new Point(1, 2);
        $this->assertEquals(1, $point->getX());
        $this->assertEquals(2, $point->getY());
        // если мы задали только один параметр, точка заменяет Y нулём
        $point = new Point(2);
        $this->assertEquals(2, $point->getX());
        $this->assertEquals(0, $point->getY());
        // если мы создаём не число, точка обнуляется целиком
        $point = new Point('lol', 2);
        $this->assertEquals(0, $point->getX());
        $this->assertEquals(0, $point->getY());
        // и наконец, проверяем является ли точка точкой
        // и что тест не является точкой
        $this->assertEquals(true, Point::isPoint($point));
        $this->assertEquals(false, Point::isPoint($this));

        $point = new Point(2, 2);
        $this->assertEquals(true, $point->isPointOnLine(1, -1, 0));
        $this->assertEquals(true, $point->isPointOnLine(1, -1, 0, 0.01));
        $this->assertEquals(true, $point->isPointOnLine(1, 1, -4));
        $this->assertEquals(true, $point->isPointOnLine(1, 1, -4, 0.01));
    }

    public function testPointSetMove() {
        // создаём точку и проверяем координаты
        $point = new Point(-6.4, 3.4);
        $this->assertEquals(-6.4, $point->getX());
        $this->assertEquals(3.4, $point->getY());
        // меняем Х
        $point->setX(55);
        $this->assertEquals(55, $point->getX());
        $this->assertEquals(3.4, $point->getY());
        // меняем Y
        $point->setY(-22);
        $this->assertEquals(55, $point->getX());
        $this->assertEquals(-22, $point->getY());
        // сдвигаем точку
        $point->movePoint(-8.3, 31.1);
        $this->assertEquals(46.7, $point->getX());
        $this->assertEquals(9.1, $point->getY());
        // считаем расстояние
        $this->assertEquals(47.578, $point->getDistance(), "Bad epsilon", 0.001);
        $this->assertEquals(44.690, $point->getDistance(2.5, 2.5), "Bad epsilon", 0.001);
    }
}
 