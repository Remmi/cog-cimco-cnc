<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 18.10.2014
 * Time: 15:37
 */

namespace Geom;


class Line {
    // параметры прямой Ах+Ву+С=0
    /**
     * @var float параметр Ах
     */
    protected $A;
    /**
     * @var float параметр By
     */
    protected $B;
    /**
     * @var float параметр С
     */
    protected $C;

    /**
     * @param $x1 - координата X1
     * @param $y1 - координата Y1
     * @param $x2 - координата Х2
     * @param $y2 - координата У2
     * @return array - параметры A, B, C
     */
    public static function calculateABC($x1, $y1, $x2, $y2) {
        $a = $y2-$y1;
        $b = $x1-$x2;
        $c = -$x1*$y2+$x2*$y1;
        return array($a, $b, $c);
    }

    /**
     * Возвращает параметр А
     * @return float - параметр А
     */
    public function getA() { return $this->A; }

    /**
     * Возвращает параметр В
     * @return float - параметр В
     */
    public function getB() { return $this->B; }

    /**
     * Возвращает параметр С
     * @return float - параметр С
     */
    public function getC() { return $this->C; }

    /**
     * @param $a - параметр А
     * @return bool
     */
    public function setA($a) {
        if (is_numeric($a)) {
            $this->A = $a;
            return true;
        }
        return false;
    }

    /**
     * @param $b - параметр B
     * @return bool
     */
    public function setB($b) {
        if (is_numeric($b)) {
            $this->B = $b;
            return true;
        }
        return false;
    }

    /**
     * @param $c - параметр C
     * @return bool
     */
    public function setC($c) {
        if (is_numeric($c)) {
            $this->C = $c;
            return true;
        }
        return false;
    }

    /**
     * Записывает все три параметра сразу
     * @param $a
     * @param $b
     * @param $c
     * @return bool
     */
    public function setABC($a=null, $b=null, $c=null) {
        if (is_numeric($a) && is_numeric($b) && is_numeric($c)) {
            if (abs($a)+abs($b) > 0) {
                $this->setA($a);
                $this->setB($b);
                $this->setC($c);
                return true;
            }
        }
        return false;
    }

    /**
     * Пересчитывает A, B и C по двум новым точкам
     * @param $x1 - X1
     * @param $y1 - Y1
     * @param $x2 - X2
     * @param $y2 - Y2
     */
    public function setABCbyXY($x1, $y1, $x2, $y2) {
        if (is_numeric($x1) && is_numeric($y1) && is_numeric($x2) && is_numeric($y2)) {
            $abc = self::calculateABC($x1, $y1, $x2, $y2);
            if (abs($abc[0])+abs($abc[1]) > 0) {
                $this->setABC($abc[0], $abc[1], $abc[2]);
            }
        }
    }

    public function getDistanceToPoint($x, $y) {
        return Point::getDistanceBetweenPointAndLine($x, $y, $this->getA(), $this->getB(), $this->getC());
    }

    public function isPointOnLine($x, $y, $e=0.0) {
        $d = $this->getDistanceToPoint($x, $y);
        return ($d <= $e);
    }

    public function getVerticalCrossToPoint($x, $y) {
        $a = $this->getA();
        $b = $this->getA();
        $c = $this->getA();
        $sPoint = new Point(0, -$c/$b);
        $ePoint = new Point(-$c/$a, 0);
        return Point::getVerticalCrossWithPoint(
            $x, $y, $sPoint->getX(), $sPoint->getY(), $ePoint->getX(), $ePoint->getY()
        );
    }

    /**
     * Создаёт экземпляр прямой по двум точкам
     * @param $x1 - X1
     * @param $y1 - Y1
     * @param $x2 - X2
     * @param $y2 - Y2
     */
    public function __construct ($x1, $y1, $x2, $y2) {
        if (is_numeric($x1) && is_numeric($y1) && is_numeric($x2) && is_numeric($y2)) {
            $abc = self::calculateABC($x1, $y1, $x2, $y2);
            if (abs($abc[0])+abs($abc[1]) > 0) {
                $this->setABC($abc[0], $abc[1], $abc[2]);
            }
        }
        return false;
    }

    /**
     * TODO: экземпляр прямой по точке и углу
     */
} 