<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 20.10.14
 * Time: 9:08
 */

namespace Geom;

include 'Point.php';
include 'Line.php';
include 'LineSegment.php';
include 'Angle.php';

class AngleTest extends \PHPUnit_Framework_TestCase {
    public function testAngleMakeTurn() {
        $sPoint = new Point(1, 2);
        $ePoint = new Point(5, 6);
        $turnAngle = Angle::countLineSegmentAngle($sPoint->getX(), $sPoint->getY(), $ePoint->getX(), $ePoint->getY());
        $this->assertEquals(45, $turnAngle);
        $offsetX = LineSegment::countLineSegmentOffsetX($sPoint->getX(), $sPoint->getY(), $ePoint->getX(), $ePoint->getY());

        $newSPoint = Angle::makeTurn($sPoint->getX(), $sPoint->getY(), 45, $turnAngle, $offsetX);
        $this->assertEquals(-1, $newSPoint[0]);
        $this->assertEquals(2.82, $newSPoint[1], "Wrong Y", 0.01);

        $newEPoint = Angle::makeTurn($ePoint->getX(), $ePoint->getY(), 45, $turnAngle, $offsetX);
        $this->assertEquals(-1, $newEPoint[0]);
        $this->assertEquals(8.48, $newEPoint[1], "Wrong Y", 0.01);
    }

    public function testAngleCountLineAngle() {
        $sPoint = new Point(-4, 3);
        $ePoint = new Point(-6, 6);
        $abc = Line::calculateABC($sPoint->getX(), $sPoint->getY(), $ePoint->getX(), $ePoint->getY());
        $angle = Angle::countLineAngle($abc[0], $abc[1]);    // -56.30993247402
        $this->assertEquals(-56.309, $angle, "Wrong angle", 0.001);
    }
}
