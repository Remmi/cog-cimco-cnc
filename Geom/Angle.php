<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 20.10.14
 * Time: 8:45
 */

namespace Geom;


class Angle {
    /**
     * @var int количество знаков округления синусов и косинусов
     */
    const e = 5;

    /**
     * Определяет угол между прямой и осью Ox
     * @param $a - Ax
     * @param $b - By
     * @return float - угол в градусах
     */
    public static function countLineAngle($a, $b) {
        return rad2deg(atan(-$a/$b));
    }

    /**
     * Определяет угол между прямой, на которой лежит отрезок, и осью Ox
     * @param $x1
     * @param $y1
     * @param $x2
     * @param $y2
     * @return float
     */
    public static function countLineSegmentAngle($x1, $y1, $x2, $y2) {
        $abc = Line::calculateABC($x1, $y1, $x2, $y2);
        return rad2deg(atan(-$abc[0]/$abc[1]));
    }

    /**
     * @param float $x
     * @param float $y
     * @param float $angle - угол поворота
     * @param float $startAngle - начальный угол у точки
     * @param float $ex - смещение по оси X (C)
     * @param float $ey - смещение по оси Y (b)
     * @return array|bool - результат поворота
     */
    public static function makeTurn($x, $y, $angle, $startAngle=0.0, $ex=0.0) {
        if (is_numeric($x) && is_numeric($y) && is_numeric($angle) && is_numeric($ex)) {
            $x = $x-$ex;
            $r = LineSegment::getDistance(new Point(0, 0), new Point($x, $y));
            $x = $r*round(cos(deg2rad($startAngle+$angle)), self::e)+$ex;
            $y = $r*round(sin(deg2rad($startAngle+$angle)), self::e);
            return array($x, $y);
        }
        return false;
    }
}