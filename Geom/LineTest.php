<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 18.10.2014
 * Time: 16:18
 */

namespace Geom;

include 'Point.php';
include 'Line.php';

class LineTest extends \PHPUnit_Framework_TestCase {
    public function testLine() {
        $line = new Line(1, 1, 2, 3);
        $this->assertEquals(2, $line->getA());
        $this->assertEquals(-1, $line->getB());
        $this->assertEquals(-1, $line->getC());

        $line->setABCbyXY(12, 13, 13, 0);
        $this->assertEquals(-13, $line->getA());
        $this->assertEquals(-1, $line->getB());
        $this->assertEquals(169, $line->getC());

        $line->setA(15);
        $this->assertEquals(15, $line->getA());
        $line->setB(58);
        $this->assertEquals(58, $line->getB());
        $line->setC(27);
        $this->assertEquals(27, $line->getC());

        $line->setABC(3, 4, 5);
        $this->assertEquals(3, $line->getA());
        $this->assertEquals(4, $line->getB());
        $this->assertEquals(5, $line->getC());

        // ничего не должно измениться
        $line->setABC(0, 0, 6);
        $this->assertEquals(3, $line->getA());
        $this->assertEquals(4, $line->getB());
        $this->assertEquals(5, $line->getC());

        $abc = Line::calculateABC(75, 117, 11, 161);
        $this->assertEquals(44, $abc[0]);
        $this->assertEquals(64, $abc[1]);
        $this->assertEquals(-10788, $abc[2]);

        $line->setABCbyXY(1, 1, 3, 3);
        $this->assertEquals(true, $line->isPointOnLine(2, 2));
        $this->assertEquals(true, $line->isPointOnLine(2, 2, 0.01));
        $this->assertEquals(true, $line->isPointOnLine(199, 199, 0.01));
    }
}
 