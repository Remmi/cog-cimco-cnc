<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 18.10.2014
 * Time: 12:36
 */

namespace Geom;


class LineSegment extends Line {
    /**
     * @var Point начальная точка
     */
    private $startPoint;
    /**
     * @var Point конечная точка
     */
    private $endPoint;

    /**
     * Возвращает начальную точку линии
     * @return Point - начальная точка
     */
    public function getStartPoint() {
        return $this->startPoint;
    }

    /**
     * Возвращает конечную точку линии
     * @return Point
     */
    public function getEndPoint() {
        return $this->endPoint;
    }

    /**
     * Меняет начальную точку для линии
     * @param Point $point - новое значение начальной точки для линии
     * @return bool
     */
    public function setStartPoint($point) {
        if (Point::isPoint($point)) {
            $this->startPoint = $point;
            $ePoint = $this->getEndPoint();
            $this->setABCbyXY(
                $point->getX(),
                $point->getY(),
                $ePoint->getX(),
                $ePoint->getY()
            );
            return true;
        }
        return false;
    }

    /**
     * Меняет конечную точку для линии
     * @param Point $point - новое значение конечной точки для линии
     * @return bool
     */
    public function setEndPoint($point) {
        if (Point::isPoint($point)) {
            $this->endPoint = $point;
            $sPoint = $this->getStartPoint();
            $this->setABCbyXY(
                $sPoint->getX(),
                $sPoint->getY(),
                $point->getX(),
                $point->getY()
            );
            return true;
        }
        return false;
    }

    /**
     * @param float $x - сдвиг по Х
     * @param float $y - сдвиг по Y
     * @return bool
     */
    public function moveLineSegment($x, $y) {
        if (is_numeric($x) && is_numeric($y)) {
            $sPoint = $this->getStartPoint();
            $ePoint = $this->getEndPoint();
            $sPoint->setX($sPoint->getX()+$x);
            $sPoint->setY($sPoint->getY()+$y);
            $ePoint->setX($ePoint->getX()+$x);
            $ePoint->setY($ePoint->getY()+$y);
            $this->setStartPoint($sPoint);
            $this->setEndPoint($ePoint);
            $this->setABCbyXY(
                $sPoint->getX(),
                $sPoint->getY(),
                $ePoint->getX(),
                $ePoint->getY()
            );
            return true;
        }
        return false;
    }

    /**
     * Возвращает координаты середины отрезка
     * @return Point точка середины отрезка
     */
    public function getMiddlePoint() {
        $sPoint = $this->getStartPoint();
        $ePoint = $this->getEndPoint();
        $point = new Point(
            ($sPoint->getX()+$ePoint->getX())/2,
            ($sPoint->getY()+$ePoint->getY())/2
        );
        return $point;
    }

    /**
     * Считает расстояние между двумя точками
     * @param Point $startPoint
     * @param Point $endPoint
     * @return int|bool расстояние между двумя точками
     */
    public static function getDistance($startPoint, $endPoint) {
        if (Point::isPoint($startPoint) && Point::isPoint($endPoint)) {
            $x1 = $startPoint->getX();
            $y1 = $startPoint->getY();
            $x2 = $endPoint->getX();
            $y2 = $endPoint->getY();
            return sqrt(($x2-$x1)*($x2-$x1)+($y2-$y1)*($y2-$y1));
        }
        return false;
    }

    public function getLength() {
        return self::getDistance(
            $this->getStartPoint(),
            $this->getEndPoint()
        );
    }

    public static function countLineSegmentOffsetX($x1, $y1, $x2, $y2) {
        $abc = Line::calculateABC($x1, $y1, $x2, $y2);
        if ($abc[0] !== 0) {
            return (-$abc[2]/$abc[0]);
        }
        return 0;
    }

    public function getOffsetX() {
        if ($this->getA() != 0) {
            return -($this->getC()/$this->getA());
        }
        return 0;
    }

    /**
     * Конструктор класса Отрезок (Line Segment)
     * Можно задать отрезок двумя точками
     * Точкой и двумя координатами
     * Двумя координатами и точкой
     * Или четырьмя координатами
     */
    public function __construct() {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            $res = call_user_func_array(array($this, $f), $a);
            if ($res) {
                $sPoint = $this->getStartPoint();
                $ePoint = $this->getEndPoint();
                $this->setABCbyXY(
                    $sPoint->getX(),
                    $sPoint->getY(),
                    $ePoint->getX(),
                    $ePoint->getY()
                );
            }
            return $res;
        }
        return false;
    }

    // Задание двумя точками
    function __construct2($startPoint, $endPoint) {
        if (Point::isPoint($startPoint) && (Point::isPoint($endPoint))) {
            $this->startPoint = $startPoint;
            $this->endPoint = $endPoint;
            return true;
        }
        return false;
    }

    // задание точкой и двумя координатами
    function __construct3($a1, $a2, $a3) {
        if (Point::isPoint($a1)) {
            $this->startPoint = $a1;
            $this->endPoint = new Point($a2, $a3);
        } elseif (Point::isPoint($a3)) {
            $this->startPoint = new Point($a1, $a2);
            $this->endPoint = $a3;
        } else {
            return false;
        }
        return true;
    }

    // задание четырьмя координатами
    function __construct4($x1, $y1, $x2, $y2) {
        $this->startPoint = new Point($x1, $y1);
        $this->endPoint = new Point($x2, $y2);
        return true;
    }
} 