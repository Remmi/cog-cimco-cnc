<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 20.10.14
 * Time: 8:57
 */

namespace Geom;


class Circle {
    private $x;

    private $y;

    private $r;

    /**
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param float $x
     * @return bool
     */
    public function setX($x)
    {
        $this->x = $x;
        return true;
    }

    /**
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param float $y
     * @return bool
     */
    public function setY($y)
    {
        $this->y = $y;
        return true;
    }

    /**
     * @return float
     */
    public function getR()
    {
        return $this->r;
    }

    /**
     * @param float $r
     * @return bool
     */
    public function setR($r)
    {
        $this->r = $r;
        return true;
    }

    public function isPointOnCircle($x, $y, $e) {
        return Point::isPointOnCircle(
            $x, $y, $this->getR(), $this->getX(), $this->getY(), $e
        );
    }

    public function getDistanceToPoint($x, $y) {
        return Point::getDistanceBetweenPointAndCircle(
            $x, $y, $this->getR(), $this->getX(), $this->getY()
        );
    }

    public static function isCirclesIntersect($r2, $x2, $y2, $r1, $x1=0, $y1=0, $e=0.01) {
        $d = Point::getDistanceBetweenPoints($x1, $y1, $x2, $y2);
        if ($d > $r1 + $r2 + $e) return 0;
        if ($d < $r1 + $r2 - $e) return 2;
        return 1;
    }

    function __construct($r, $x=0.0, $y=0.0) {
        $this->setR($r);
        $this->setX($x);
        $this->setY($x);
        return true;
    }
}