<?php
bcscale(10);

$z = 75;                // количество зубов
$t = 19.05;              // шаг цепи
$d1 = 11.91;            // диаметр ролика или втулки
$B_out = 12.7;          // внутренняя ширина цепи
$h = 18.2;              // высота звена цепи

$Dd = 454.9182712034;           // диаметр делит.окружности
$De = 464.6538305884;            // диаметр окружности выступов
$Di = 442.8487212034;           // диаметр окружности впадин

$r3 = 20.2470000000;             // радиус закругления зуба
$H = 9.5280000000;              // расстояние от вершины зуба
$m = 11.6610000000;             // ширина зуба
$Dc = 430.8592305884;            // диаметр ступицы (наибольший)

$r = 6.0347750000;//6.03;             // радиус впадин
$I = 1.5994962217;            // геометр.характеристика зацепл.
$K = 0.532;             // коэффициент высоты зуба
$Lx = 442.7375569603;           // наибольшая хорда (для нечетн z)
$r1 = 15.5627750000;               // радиус сопряжения
$r2 = 8.0628986182;             // радиус головки зуба
$alpha = 54.2;         // половина угла впадины
$betta = 7.2533333333;          // угол сопряжения
$phi = 16.1466666667;           // половина угла зуба
$FC = 2.9040756004;             // прямой участок профиля FC
$distanceO_O2 = 14.7684000000;  // расстояние между центром О и О2
$e = 0.5715000000;              // смещение дуг впадин

$eSlavaX = 0;//-6;          // поправка по Х для О2 от Славы
$eSlavaY = 0;//.6;//0.6;         // поправка по У для О2 от Славы

$O1 = array(
    'x' => 7.7278160673,//7.73,
    'y' => 5.5734767273//5.57
);

$O2 = array(
    'x' => 14.7554456040,//14.76,
    'y' => 0.6184364045//0.62
);

// поперечное сечение зуба
$r4 = 1.6;      // радиус закруглений


function makeCircle($r, $x=0, $y=0) {
    return "CIR ($x, $y, ".$r.")\r\n";
}

function makePoint($x, $y) {
    return "POI ($x, $y)\r\n";
}

function makeLine($x1, $y1, $x2, $y2) {
    return "LIN ($x1, $y1, $x2, $y2)\r\n";
}

function findABC($x1, $y1, $x2, $y2) {
    return array(
        bcsub($y2, $y1),
        bcsub($x1, $x2),
        bcsub(bcmul($x2, $y1), bcmul($x1, $y2))
    );
}

function eq_roots($a, $b, $c) {
    if ($a==0) return false;

    if ($b==0) {
        if ($c<0) {
            $x1 = bcsqrt(abs(bcdiv($c, $a)));
            $x2 = bcsqrt(abs(bcdiv($c, $a)));
        } elseif ($c==0) {
            $x1 = $x2 = 0;
        } else {
            $x1 = bcsqrt(bcdiv($c,$a)).'i';
            $x2 = -bcsqrt(bcdiv($c,$a)).'i';
        }
    } else {
        $d = bcsub(bcmul($b,$b),bcmul(4, bcmul($a,$c)));
        if ($d>0) {
            $x1 = (-$b+sqrt($d))/2*$a;
            $x2 = (-$b-sqrt($d))/2*$a;
        } elseif ($d==0) {
            $x1 = $x2 = (-$b)/2*$a;
        } else {
            $x1 = -$b . '+' . sqrt(abs($d)) . 'i';
            $x2 = -$b . '-' . sqrt(abs($d)) . 'i';
        }
    }
    return array($x1, $x2);
}

function sqr($x) {
    return bcmul($x,$x);
}

function makeRad($angle) {
    return bcdiv(bcmul($angle, pi()), 180);
}

function makeTurn($r, $angle) {
    return array(
        bcmul($r, cos(makeRad($angle))),
        bcmul($r, sin(makeRad($angle)))
    );
}

function makePointTurn($x, $y, $angle) {
    return array(
        bcmul($x, cos(makeRad($angle))),
        bcmul($y, sin(makeRad($angle)))
    );
}

/*function findIntersectLineCircle(
    $a, $b, $c,     // коэффициенты уравнения прямой
    $r, $x=0, $y=0  // параметры окружности
) {
    $eps = 0.00001; // погрешность
    $x0 = bcdiv(bcmul(-1, bcmul($a, $c)), bcadd(sqr($a), sqr($b)));
    $y0 = -$b*$c/(sqr($a)+sqr($b));

    if (sqr($c) > sqr($r)*(sqr($a)+sqr($b))+$eps) {
        echo sqr($c)." > ".(sqr($r)*(sqr($a)+sqr($b))+$eps)."\r\n";
        return false;
    }
    else if (abs(sqr($c) - sqr($r)*(sqr($a)+sqr($b))) < $eps) {
        return array($x0+$x, $y0+$y);
    }
    else {
        $d = sqr($r) - sqr($c)/(sqr($a)+sqr($b));
	    $mult = sqrt ($d / (sqr($a)+sqr($b)));
	    $ax = $x0 + $b * $mult;
	    $bx = $x0 - $b * $mult;
	    $ay = $y0 - $a * $mult;
	    $by = $y0 + $a * $mult;
        return array(
            $ax, $ay,
            $bx, $by
        );
    }
}

function findIntersectCircleCircle(
    $r2, $x2, $y2,
    $r1, $x1=0, $y1=0
) {
    $a = bcmul(-2, $x2);
    $b = bcmul(-2, $y2);
    $c = bcadd(bcadd(sqr($x2),sqr($y2)), bcsub(sqr($r1), sqr($r2)));
    if (($x1 == $x2) && ($y1 == $y2)) {
        return false;
    }
    return findIntersectLineCircle($a, $b, $c, $r1, $x1, $y1);
}*/

function makeSymmetry($x, $y, $a, $b, $c, $half=false) {
    $t = bcmul(-1, (bcadd(bcadd($c,$x),$y)))/(bcadd(bcmul($a, $a), bcmul($b, $b)));
    $x1 = ($half) ? bcadd(bcmul($a,$t), $x) : bcadd(bcmul(bcmul($a, $t), 2), $x);
    $y1 = ($half) ? bcadd(bcmul($b,$t), $y) : bcadd(bcmul(bcmul($b, $t),2), $y);
    return array($x1, $y1);
}

function makeZub($file, $Ox, $Oy, $Or, $O1x, $O1y, $O1r, $O2x, $O2y, $O2r) {
    // рисуем О
    fwrite($file, makePoint($Ox, $Oy));
    fwrite($file, makeCircle($Or, $Ox, $Oy));
    // рисуем О1
    fwrite($file, makePoint(bcadd($Ox, $O1x), bcsub($Oy, $O1y)));
    fwrite($file, makeCircle($O1r, bcadd($Ox, $O1x), bcsub($Oy, $O1y)));
    // рисуем О2
    fwrite($file, makePoint(bcadd($Ox, $O2x), bcadd($Oy, $O2y)));
    fwrite($file, makeCircle($O2r, bcadd($Ox, $O2x), bcadd($Oy, $O2y)));
    return true;
}

function makeNewZub(
    $file,
    $Ox, $Oy, $Or,      // центральная окружность О
    $O1x, $O1y, $O1r,   // О1
    $O11x, $O11y,       // отражение О1
    $O2x, $O2y, $O2r,   // О2
    $O21x, $O21y        // отражение О2
) {
    fwrite($file, makeCircle($Or, $Ox, $Oy));
    // рисуем О1
    fwrite($file, makeCircle($O1r, $O1x, $O1y));
    fwrite($file, makeCircle($O1r, $O11x, $O11y));
    // рисуем О2
    fwrite($file, makeCircle($O2r, $O2x, $O2y));
    fwrite($file, makeCircle($O2r, $O21x, $O21y));
}



$file = fopen('echo-75-2.cdd', 'w');
fwrite($file, "CNC-Calc 2000\r\nISO Milling\r\n");

// основные окружности для отрисовки звездочки
fwrite($file, makeCircle(bcdiv($Di,2)));
fwrite($file, makeCircle(bcdiv($Dd,2)));
fwrite($file, makeCircle(bcdiv($De,2)));

// вычисляем точку О
$O = array(
  'x' => bcadd(bcdiv($Di,2),$r),
  'y' => 0//$e/2
);

// точка О - $O['x'], $O['y']
// точка О1 - $O['x']+$O1['y'], $O['y']-$O1['x']
$O1x = bcadd($O['x'], $O1['y']);
$O1y = bcsub($O['y'], $O1['x']);
$O11x = bcadd($O['x'], $O1['y']);
$O11y = bcsub($O1['x'], $O['y']);
// точка О2 - $O['x']+$O2['y']+$eSlavaX, $O['y']+$O2['x']+$eSlavaY
$O2x = bcsub($O['x'], $O2['y']);
$O2y = bcadd($O['y'], $O2['x']);
$O21x = bcadd($O['x'], $O2['y']);
$O21y = bcmul(-1, (bcadd($O['y'], $O2['x'])));

fwrite($file, makeCircle($r1, $O1x, $O1y));
//fwrite($file, makeCircle($r1, $O11x, $O11y));

fwrite($file, makeCircle($r2, $O2x, $O2y));
//fwrite($file, makeCircle($r2, $O21x, $O21y));

for ($i = 0; $i < $z; $i++) {
    $O = makeTurn(bcdiv($Dd, 2), bcmul(360, bcdiv($i,$z)));
    fwrite($file, makeCircle($r, $O[0], $O[1]));
}


for ($i = 0; $i < $z*2; $i++) {
    $O = makeTurn($De/2, 360*$i/($z*2));
    fwrite($file, makeLine(0, 0, $O[0], $O[1]));
    $O1 = makePointTurn($O1x, $O1y, bcmul(360, bcdiv($i,$z)));
    $O11 = makePointTurn($O11x, $O11y, bcmul(360, bcdiv($i,$z)));
    $O2 = makePointTurn($O2x, $O2y, bcmul(360, bcdiv($i,$z)));
    $O21 = makePointTurn($O21x, $O21y, bcmul(360, bcdiv($i,$z)));

    //var_dump($O1);

    //makeNewZub(
    //    $file,
    //    $O[0], $O[1], $r,
    //    $O1[0], $O1[1], $r1,
    //    $O11[0], $O1[1],
    //    $O2[0], $O2[1], $r2,
    //    $O21[0], $O21[1]
    //);
}

/*
// отражаем О
fwrite($file, makePoint($O['x'], $O['y']));
fwrite($file, makeCircle($r, $O['x'], $O['y']));
// отражаем О1
fwrite($file, makePoint($O['x']+$O1['y'],$O['y']+$O1['x']));
fwrite($file, makeCircle($r1, $O['x']+$O1['y'], $O['y']+$O1['x']));
// отражаем О2
fwrite($file, makePoint($O['x']+$O2['y']+$eSlavaX, $O['y']-$O2['x']-$eSlavaY));
fwrite($file, makeCircle($r2, $O['x']+$O2['y']+$eSlavaX, $O['y']-$O2['x']-$eSlavaY));*/