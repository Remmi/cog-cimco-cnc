<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 04.11.14
 * Time: 16:44
 */

bcscale(10);

function getDistance($x1, $y1, $x2=0.0, $y2=0.0) {
    return bcsqrt(bcadd(bcmul(bcsub($x1, $x2), bcsub($x1, $x2)), bcmul(bcsub($y1, $y2), bcsub($y1, $y2))));
}

$d = getDistance(230.363585668165600, 30.139906571374460, 232.078, 10.758);

$res = abs(bcsub($d, 19.458));

var_dump($res);