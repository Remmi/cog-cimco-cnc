<?php

$sourceFile = file('zvezda-filter.cdd');

$outputFile = fopen('gcode.cdd', 'w');

$params = array(
    2 => array('g'=>'G03', 'r'=>"R6.03"),
    3 => array('g'=>'G03', 'r'=>"R15.6"),
    4 => array('g'=>'G01', 'r'=>null),
    5 => array('g'=>'G02', 'r'=>"R8.1"),
    6 => array('g'=>'G01', 'r'=>null),
    7 => array('g'=>'G02', 'r'=>"R8.1"),
    0 => array('g'=>'G01', 'r'=>null),
    1 => array('g'=>'G03', 'r'=>"R15.6"),
);

foreach ($sourceFile as $num => $str) {
    if ($num > 1) {
        $tmpStr = explode(' ', $str);
        $tmpStr[0] = trim($tmpStr[0], "POI(,");
        $tmpStr[1] = trim($tmpStr[1], "\r\n)");
        $x = "X".round($tmpStr[0], 3);
        $y = "Y".round($tmpStr[1], 3);
        $g = $params[$num%8]['g'];
        $r = $params[$num%8]['r'];
        $resStr = $g." ".$x." ".$y." ".$r."\r\n";
        fwrite($outputFile, $resStr);
    }
}
