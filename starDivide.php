<?php

class Point {
    private $x = 0;
    private $y = 0;

    private $isFree = true;

    public function __construct($x=null, $y=null) {
        if (!is_null($x)) {
            $this->x = $x;
        }

        if (!is_null($y)) {
            $this->y = $y;
        }
    }

    public function getDistance($x, $y) {
        return sqrt(($this->x-$x)*($this->x-$x)+($this->y-$y)*($this->y-$y));
    }

    public function getDistancePoint($x) {
        if (Point::isPoint($x)) {
            $coord = $x->getXY();
            return sqrt(($this->x-$coord[0])*($this->x-$coord[0])+($this->y-$coord[1])*($this->y-$coord[1]));
        }
    }

    public function getXY() {
        return array($this->x, $this->y);
    }

    public static function isPoint($x) {
        return ($x instanceof Point);
    }

    public function isFree() {
        return ($this->isFree);
    }

    public function makeBelong() {
        $this->isFree = false;
    }

    public function findMiddle($x) {
        if (Point::isPoint($x)) {
            $coord = $x->getXY();
            return array($this->x+$coord[0]/2, $this->y+$coord[1]/2);
        }
    }
}

class Arc {
    private $points = array();

    public function addPoint($value, $position=null) {
        if (Point::isPoint($value)) {
            if (is_null($position)) {
                $position = count($this->points);
            }
            $this->points[$position] = $value;
            $value->makeBelong();
        }
    }

    public function getPoint($position=0) {
        return $this->points[$position];
    }

    public function getPoints() {
        return $this->points;
    }

    public function countPoints() {
        return count($this->points);
    }

    public function connectArc($x) {
        if ($x instanceof Arc) {
            // надо понять, какой правый, какой левый
            $currTop = $this->getPoint(3);
            $xTop = $x->getPoint(3);
            $middleTop = $currTop->findMiddle($xTop);
            unset($x);
        }
    }

    public function makeReverse() {
        array_reverse($this->points);
    }
}

$points = array();

$arcs = array();

// исходный файл с ЧПУ
$sourceFile = file('zvezda.cdd');

$outputFile = fopen('zvezda-point.txt', 'w');

$i = 0;
foreach ($sourceFile as $num => $str) {
    $tmpStr = explode(' ', $str);
    if ($tmpStr[0] == 'POI') {
        $i++;
        $tmpStr[1] = substr($tmpStr[1], 1, -1);
        $point = explode(',', $tmpStr[1]);
        trim($tmpStr[2], "\n");
        $tmpStr[2] = substr($tmpStr[2], 0, -3);
        $x = round($tmpStr[1], 3);
        $y = round($tmpStr[2], 3);
        $resStr = ($i == 1) ? "X$x"." "."Y$y" : "\r\n"."X$x"." "."Y$y";
        array_push($points, new Point($x, $y));
        //fwrite($outputFile, $resStr);
    }
}

echo "$i points were found\n";
unset($sourceFile);

foreach ($points as $currPoint) {
    if ($currPoint->getDistance(0, 0) < 158) {
        $arc = new Arc();
        $arc->addPoint($currPoint);
        array_push($arcs, $arc);
    }
}

foreach ($arcs as $num => $currArc) {
    $currArcPoint = $currArc->getPoint(0);
    $i = 0;
    foreach ($points as $currPoint) {
        if ($currPoint->isFree() && $currPoint->getDistancePoint($currArcPoint) < 15) {
            $currArc->addPoint($currPoint);
            break;
        }
    }

    foreach ($points as $currPoint) {
        if ($currPoint->isFree() && $currPoint->getDistancePoint($currArcPoint) < 15) {
            $currArc->addPoint($currPoint);
            $currArcPoint = $currPoint;
            break;
        }
    }

    foreach ($points as $currPoint) {
        if ($currPoint->isFree() && $currPoint->getDistancePoint($currArcPoint) < 11) {
            $currArc->addPoint($currPoint);
            $currArcPoint = $currPoint;
            break;
        }
    }
}

foreach ($arcs as $num => $currArc) {
    if ($currArc->countPoints() < 4) {
        unset($arcs[$num]);
    }
}
$arcs = array_values($arcs);
$maxCCC = count($arcs);

$firstArc = null;
$secondArc = null;

$ccc = 0;
do {
    foreach ($arcs as $num => $currArc) {
        $currFirstPoint = $currArc->getPoint(0);
        $currLastPoint = $currArc->getPoint(3);
        // теперь надо сравнить расстояния между первыми и последними точками между всеми арками
        for ($i = $num + 1; $i < count($arcs); $i++) {
            // если два арка соединяются
            if (($currFirstPoint->getDistancePoint($arcs[$i]->getPoint(3))) ||
                ($currLastPoint->getDistancePoint($arcs[$i]->getPoint(0)))
            ) {
                $firstArc = $arcs[$num];
                $secondArc = $arcs[$i];
                if ($currFirstPoint->getDistancePoint($arcs[$i]->getPoint(3))) {
                    $firstArc->makeReverse();
                } else {
                    $secondArc->makeReverse();
                }
                unset($arcs[$num]);
                unset($arcs[$i]);

                break;
            }
        }

        $firstArc->connectArc($secondArc);
        foreach ($firstArc->getPoints() as $cP) {
            $coord = $cP->getXY();
            fwrite($outputFile, "X" . $coord[0] . " Y" . $coord[1] . "\r\n");
        }
        fwrite($outputFile, "\r\n");
        break;
    }
    $ccc++;
} while ($ccc < $maxCCC);

/*foreach ($arcs as $num => $currArc) {
    $currArcPoints = $currArc->getPoints();
    echo "Arc $num has ".$currArc->countPoints()." points\r\n";
    foreach ($currArcPoints as $cP) {
        $coord = $cP->getXY();
        fwrite($outputFile, "X".$coord[0]." Y".$coord[1]."\r\n");
    }
    fwrite($outputFile, "\r\n");
}*/



