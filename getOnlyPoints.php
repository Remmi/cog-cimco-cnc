<?php
/**
 * Created by PhpStorm.
 * User: Roman Boykov
 * Date: 04.11.14
 * Time: 10:18
 */

// точность округления в BC
bcscale(10);

$settingsFile = 'settings-75-9.json';

$hash = array();
$points = array();

function getDistance($x1, $y1, $x2=0.0, $y2=0.0) {
    return bcsqrt(bcadd(bcmul(bcsub($x1, $x2), bcsub($x1, $x2)), bcmul(bcsub($y1, $y2), bcsub($y1, $y2))));
}

function countHash($x, $y) {
    return md5(round($x,2).round($y,2));
}

$settings = json_decode(file_get_contents($settingsFile), true);

$sourceFile = file($settings['file']['source']);

$Dd = $settings['Dd'];           // диаметр делит.окружности
$De = $settings['De'];            // диаметр окружности выступов
$Di = $settings['Di'];           // диаметр окружности впадин
$eps = (array_key_exists('eps', $settings)) ? $settings['eps'] : 0.1;

// расстояния между точками на дуге (снизу вверх: 0-1-2-3 4-5-6-7
$d30 = $settings['distance']['3-0'];
$d31 = $settings['distance']['3-1'];
$d32 = $settings['distance']['3-2'];
$d34 = $settings['distance']['3-4'];
$d35 = $settings['distance']['3-5'];
$d36 = $settings['distance']['3-6'];
$d37 = $settings['distance']['3-7'];
// между краями новых дуг
$d33 = $settings['distance']['3-3'];

$i = 0;

$firstX = -999;
$firstY = -999;


foreach ($sourceFile as $num => $str) {
    $tmpStr = explode(' ', $str);
    if ($tmpStr[0] == 'POI') {
        $tmpStr[1] = substr($tmpStr[1], 1, -1);
        $point = explode(',', $tmpStr[1]);
        trim($tmpStr[2], "\n");
        $tmpStr[2] = substr($tmpStr[2], 0, -3);
        $x = $tmpStr[1];
        $y = $tmpStr[2];
        $resStr = ($i == 1) ? "$x"."; "."$y" : "\r\n"."$x"."; "."$y";
        if (!array_key_exists(md5(round($x, 2).round($y, 2)), $hash)) {
            $i++;
            $hash[countHash($x, $y)] = true;
            array_push($points, array($x, $y));
            $distance = getDistance($x, $y);
            if (abs(bcsub($distance, bcdiv($De,2))) < $eps) {
                if (($x > $firstX) && ($y < 0) && (abs($y) < abs($firstY))) {
                    $firstX = $x;
                    $firstY = $y;
                }
            }
        }
    }
}

$countPoints = $i;

$outputFile2 = fopen($settings['file']['output'], 'w');
fwrite($outputFile2, "CNC-Calc 2000\r\nISO Milling\r\n");
$isFoundFirst = false;

while ($i > 0) {
    $hash[countHash($firstX, $firstY)] = false;
    $zub[3] = array($firstX, $firstY);
    foreach ($points as $index => $currPoint) {
        if ($hash[countHash($currPoint[0], $currPoint[1])]) {
            $currDistance = getDistance($currPoint[0], $currPoint[1], $zub[3][0], $zub[3][1]);
            if (abs(bcsub($currDistance, $d30)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[2] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d31)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[1] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d32)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[0] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d34)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[4] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d35)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[5] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d36)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[6] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d37)) < $eps) {
                $hash[countHash($currPoint[0], $currPoint[1])] = false;
                $zub[7] = array($currPoint[0], $currPoint[1]);
            }
            if (abs(bcsub($currDistance, $d33)) < $eps) {
                if ($i == 600) {
                    if (($currPoint[0]*$zub[3][0] >= 0) && ($currPoint[1]*$zub[3][1] >= 0)) {
                        $isFoundFirst = true;
                    }
                } else {
                    $isFoundFirst = true;
                }

                if ($isFoundFirst) {
                    $hash[countHash($currPoint[0], $currPoint[1])] = false;
                    $firstX = $currPoint[0];
                    $firstY = $currPoint[1];
                }
            }
        }
    }

    for ($j = 0; $j < 8; $j++) {
        $str = "POI(" . $zub[$j][0] . ", " . $zub[$j][1] . ")\r\n";
        fwrite($outputFile2, $str);
        $i--;
    }
    unset($zub);

    echo $i."\r\n";
}


