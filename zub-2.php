<?php

$z = 12;                // количество зубов
$t = 38.1;              // шаг цепи
$d1 = 22.23;            // диаметр ролика или втулки
$B_out = 25.4;          // внутренняя ширина цепи
$h = 36.2;              // высота звена цепи

$Dd = 147.21;           // диаметр делит.окружности
$De = 163.7;            // диаметр окружности выступов
$Di = 124.77;           // диаметр окружности впадин

$r3 = 37.8;             // радиус закругления зуба
$H = 17.8;              // расстояние от вершины зуба
$m = 23.47;             // ширина зуба
$Dc = 98.8;             // диаметр ступицы (наибольший)

$r = 11.22;             // радиус впадин
$I = 1.7139;            // геометр.характеристика зацепл.
$K = 0.565;             // коэффициент высоты зуба
$Lx = 123.37;           // наибольшая хорда (для нечетн z)
$r1 = 29;               // радиус сопряжения
$r2 = 15.7;             // радиус головки зуба
$alpha = 50;         // половина угла впадины
$betta = 3.33;          // угол сопряжения
$phi = 11.67;           // половина угла зуба
$FC = 4.54;             // прямой участок профиля FC
$distanceO_O2 = 27.57;  // расстояние между центром О и О2
$e = 1.14;              // смещение дуг впадин

$eSlavaX = -10;          // поправка по Х для О2 от Славы
$eSlavaY = 4;         // поправка по У для О2 от Славы

$O1 = array(
    'x' => 13.62,
    'y' => 11.43
);

$O2 = array(
    'x' => 26.63,
    'y' => 7.13
);

// поперечное сечение зуба
$r4 = 2.5;      // радиус закруглений


function makeCircle($r, $x=0, $y=0) {
    return "CIR ($x, $y, ".$r.")\r\n";
}

function makePoint($x, $y) {
    return "POI ($x, $y)\r\n";
}

function makeLine($x1, $y1, $x2, $y2) {
    return "LIN ($x1, $y1, $x2, $y2)\r\n";
}

function findABC($x1, $y1, $x2, $y2) {
    return array(
        $y2-$y1,
        $x1-$x2,
        -$x1*$y2+$x2*$y1
    );
}

function eq_roots($a, $b, $c) {
    if ($a==0) return false;

    if ($b==0) {
        if ($c<0) {
            $x1 = sqrt(abs($c/$a));
            $x2 = sqrt(abs($c/$a));
        } elseif ($c==0) {
            $x1 = $x2 = 0;
        } else {
            $x1 = sqrt($c/$a).'i';
            $x2 = -sqrt($c/$a).'i';
        }
    } else {
        $d = $b*$b-4*$a*$c;
        if ($d>0) {
            $x1 = (-$b+sqrt($d))/2*$a;
            $x2 = (-$b-sqrt($d))/2*$a;
        } elseif ($d==0) {
            $x1 = $x2 = (-$b)/2*$a;
        } else {
            $x1 = -$b . '+' . sqrt(abs($d)) . 'i';
            $x2 = -$b . '-' . sqrt(abs($d)) . 'i';
        }
    }
    return array($x1, $x2);
}

function sqr($x) {
    return $x*$x;
}

function makeRad($angle) {
    return $angle*pi()/180;
}

function makeTurn($r, $angle) {
    return array(
        $r*cos(makeRad($angle)),
        $r*sin(makeRad($angle))
    );
}

function makePointTurn($x, $y, $angle) {
    return array(
        $x*cos(makeRad($angle)),
        $y*sin(makeRad($angle))
    );
}

function findIntersectLineCircle(
    $a, $b, $c,     // коэффициенты уравнения прямой
    $r, $x=0, $y=0  // параметры окружности
) {
    $eps = 0.00001; // погрешность
    $x0 = -$a*$c/(sqr($a)+sqr($b));
    $y0 = -$b*$c/(sqr($a)+sqr($b));

    if (sqr($c) > sqr($r)*(sqr($a)+sqr($b))+$eps) {
        echo sqr($c)." > ".(sqr($r)*(sqr($a)+sqr($b))+$eps)."\r\n";
        return false;
    }
    else if (abs(sqr($c) - sqr($r)*(sqr($a)+sqr($b))) < $eps) {
        return array($x0+$x, $y0+$y);
    }
    else {
        $d = sqr($r) - sqr($c)/(sqr($a)+sqr($b));
	    $mult = sqrt ($d / (sqr($a)+sqr($b)));
	    $ax = $x0 + $b * $mult;
	    $bx = $x0 - $b * $mult;
	    $ay = $y0 - $a * $mult;
	    $by = $y0 + $a * $mult;
        return array(
            $ax, $ay,
            $bx, $by
        );
    }
}

function findIntersectCircleCircle(
    $r2, $x2, $y2,
    $r1, $x1=0, $y1=0
) {
    $a = -2*$x2;
    $b = -2*$y2;
    $c = sqr($x2)+sqr($y2)+sqr($r1)-sqr($r2);
    if (($x1 == $x2) && ($y1 == $y2)) {
        return false;
    }
    return findIntersectLineCircle($a, $b, $c, $r1, $x1, $y1);
}

function makeSymmetry($x, $y, $a, $b, $c, $half=false) {
    $t = (-$c-$x-$y)/(sqr($a)+sqr($b));
    $x1 = ($half) ? $a*$t+$x : $a*$t*2+$x;
    $y1 = ($half) ? $b*$t+$y : $b*$t*2+$y;
    return array($x1, $y1);
}

function makeZub($file, $Ox, $Oy, $Or, $O1x, $O1y, $O1r, $O2x, $O2y, $O2r) {
    // рисуем О
    fwrite($file, makePoint($Ox, $Oy));
    fwrite($file, makeCircle($Or, $Ox, $Oy));
    // рисуем О1
    fwrite($file, makePoint($Ox+$O1x,$Oy-$O1y));
    fwrite($file, makeCircle($O1r, $Ox+$O1x, $Oy-$O1y));
    // рисуем О2
    fwrite($file, makePoint($Ox+$O2x, $Oy+$O2y));
    fwrite($file, makeCircle($O2r, $Ox+$O2x, $Oy+$O2y));

    return true;
}

function makeNewZub(
    $file,
    $Ox, $Oy, $Or,      // центральная окружность О
    $O1x, $O1y, $O1r,   // О1
    $O11x, $O11y,       // отражение О1
    $O2x, $O2y, $O2r,   // О2
    $O21x, $O21y        // отражение О2
) {
    fwrite($file, makeCircle($Or, $Ox, $Oy));
    // рисуем О1
    fwrite($file, makeCircle($O1r, $O1x, $O1y));
    fwrite($file, makeCircle($O1r, $O11x, $O11y));
    // рисуем О2
    fwrite($file, makeCircle($O2r, $O2x, $O2y));
    fwrite($file, makeCircle($O2r, $O21x, $O21y));
}



$file = fopen('echo2.cdd', 'w');
fwrite($file, "CNC-Calc 2000\r\nISO Milling\r\n");

// основные окружности для отрисовки звездочки
fwrite($file, makeCircle($Di/2));
fwrite($file, makeCircle($Dd/2));
fwrite($file, makeCircle($De/2));

// вычисляем точку О
$O = array(
  'x' => $Di/2+$r,
  'y' => 0//$e/2
);

/*makeZub(
    $file,
    $O['x'], $O['y'], $r,
    $O1['y'], $O1['x'], $r1,
    $O2['y']+$eSlavaX, $O2['x']+$eSlavaY, $r2
);*/

// точка О - $O['x'], $O['y']
// точка О1 - $O['x']+$O1['y'], $O['y']-$O1['x']
$O1x = $O['x']+$O1['y'];
$O1y = $O['y']-$O1['x'];
$O11x = $O['x']+$O1['y'];
$O11y = -($O['y']-$O1['x']);
// точка О2 - $O['x']+$O2['y']+$eSlavaX, $O['y']+$O2['x']+$eSlavaY
$O2x = $O['x']+$O2['y']+$eSlavaX;
$O2y = $O['y']+$O2['x']+$eSlavaY;
$O21x = $O['x']+$O2['y']+$eSlavaX;
$O21y = -($O['y']+$O2['x']+$eSlavaY);

fwrite($file, makeCircle($r1, $O1x, $O1y));
fwrite($file, makeCircle($r1, $O11x, $O11y));

fwrite($file, makeCircle($r2, $O2x, $O2y));
fwrite($file, makeCircle($r2, $O21x, $O21y));

var_dump($r, $O['x'], $O['y']);
var_dump($r1, $O1x, $O1y);

for ($i = 0; $i < $z; $i++) {
    $O = makeTurn($Dd/2, 360*$i/$z);
    fwrite($file, makeCircle($r, $O[0], $O[1]));
}


for ($i = 0; $i < $z*2; $i++) {
    $O = makeTurn($De/2, 360*$i/($z*2));
    fwrite($file, makeLine(0, 0, $O[0], $O[1]));
    $O1 = makePointTurn($O1x, $O1y, 360*$i/$z);
    $O11 = makePointTurn($O11x, $O11y, 360*$i/$z);
    $O2 = makePointTurn($O2x, $O2y, 360*$i/$z);
    $O21 = makePointTurn($O21x, $O21y, 360*$i/$z);

    //var_dump($O1);

    //makeNewZub(
    //    $file,
    //    $O[0], $O[1], $r,
    //    $O1[0], $O1[1], $r1,
    //    $O11[0], $O1[1],
    //    $O2[0], $O2[1], $r2,
    //    $O21[0], $O21[1]
    //);
}

/*
// отражаем О
fwrite($file, makePoint($O['x'], $O['y']));
fwrite($file, makeCircle($r, $O['x'], $O['y']));
// отражаем О1
fwrite($file, makePoint($O['x']+$O1['y'],$O['y']+$O1['x']));
fwrite($file, makeCircle($r1, $O['x']+$O1['y'], $O['y']+$O1['x']));
// отражаем О2
fwrite($file, makePoint($O['x']+$O2['y']+$eSlavaX, $O['y']-$O2['x']-$eSlavaY));
fwrite($file, makeCircle($r2, $O['x']+$O2['y']+$eSlavaX, $O['y']-$O2['x']-$eSlavaY));*/